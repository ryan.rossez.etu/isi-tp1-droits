#!/bin/bash


# Création de dir_c 
# dir_c par rapport a groupe_a et groupe_b : seulement droit de lecture des fichiers 
# dir_c par rapport a user admin : droit de creer , supprimer, rennommer et lire les fichiers 

su admin
mkdir dir_c
touch file_testC

# Création de dir_a 
# dir_a par rapport a groupe_a : Tout les droits 
# dir_a par rapport a admin : supprimer / renommer 
# dir_a par rapport a groupe_b : aucun droit 

su lambda_a
mkdir dir_a 
chmod g+ws dir_a # Permet a tout le groupe a d'avoir tout les même droit sur tout les fichiers 
chmod o-rx dir_a # Interdit au groupe b de faire quoi que ce soit 
touch file_testA

# Création de dir_b
# dir_a par rapport a groupe_b : Tout les droits 
# dir_a par rapport a admin : supprimer / renommer 
# dir_a par rapport a groupe_a : aucun droit 

su lambda_b 
mkdir dir_b 
chmod g+ws dir_b # Permet a tout le groupe a d'avoir tout les même droit sur tout les fichiers 
chmod o-rx dir_b # Interdit au groupe a de faire quoi que ce soit 
touch file_testB