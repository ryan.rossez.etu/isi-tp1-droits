#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

echo -e "${NC}Verification pour le dossier DIR_C"
# il peut rentrer dans dir_c 
if [[ -x dir_c ]]
then
    echo -e "${GREEN}Droit d'acces au dossier : check"
else
    echo -e "${RED}Droit d'acces au dossier : uncheck"
fi

# il peut supprimer un fichier dans dir_c 
# il peut creer un fichier dans dir_c 
if [[ -w dir_c ]]
then
    echo -e "${GREEN}Droit de creation des fichiers : check"
    echo -e "${GREEN}Droit de suppression des fichiers : check"
else
    echo -e "${RED}Droit de creation des fichiers : uncheck"
    echo -e "${RED}Droit de suppression des fichiers : uncheck"
fi

for f in dir_c/*; do

    # il peut lire un fichier dans dir_c 
    if [[ -r $f ]]
    then 
        echo -e "${GREEN}Droit de lecture des fichiers : check"
    else
        echo -e "${RED}Droit de lecture des fichiers : uncheck"
    fi

    # il peut modifier un fichier dans dir_c 
    if [[ -w $f ]]
    then 
        echo -e "${GREEN}Droit d'ecriture des fichiers : check"
    else
        echo -e "${RED}Droit d'ecriture des fichiers : uncheck"
    fi

    break
done


echo -e "${NC}Verification pour le dossier DIR_A"
# il peut rentrer dans dir_A
if [[ -x dir_a ]]
then
    echo -e "${GREEN}Droit d'acces au dossier : check"
else
    echo -e "${RED}Droit d'acces au dossier : uncheck"
fi

# il peut supprimer un fichier dans dir_a
# il peut creer un fichier dans dir_a
if [[ -w dir_a ]]
then
    echo -e "${GREEN}Droit de creation des fichiers : check"
    echo -e "${GREEN}Droit de suppression des fichiers : check"
else
    echo -e "${RED}Droit de creation des fichiers : uncheck"
    echo -e "${RED}Droit de suppression des fichiers : uncheck"
fi

for f in dir_a/*; do

    # il peut lire un fichier dans dir_a
    if [[ -r $f ]]
    then 
        echo -e "${GREEN}Droit de lecture des fichiers : check"
    else
        echo -e "${RED}Droit de lecture des fichiers : uncheck"
    fi

    # il peut modifier un fichier dans dir_a
    if [[ -w $f ]]
    then 
        echo -e "${GREEN}Droit d'ecriture des fichiers : check"
    else
        echo -e "${RED}Droit d'ecriture des fichiers : uncheck"
    fi

    break
done


echo -e "${NC}Verification pour le dossier DIR_B"
# il peut rentrer dans dir_b
if [[ -x dir_b ]]
then
    echo -e "${GREEN}Droit d'acces au dossier : check"
else
    echo -e "${RED}Droit d'acces au dossier : uncheck"
fi

# il peut supprimer un fichier dans dir_b
# il peut creer un fichier dans dir_b
if [[ -w dir_b ]]
then
    echo -e "${GREEN}Droit de creation des fichiers : check"
    echo -e "${GREEN}Droit de suppression des fichiers : check"
else
    echo -e "${RED}Droit de creation des fichiers : uncheck"
    echo -e "${RED}Droit de suppression des fichiers : uncheck"
fi

for f in dir_b/*; do

    # il peut lire un fichier dans dir_b
    if [[ -r $f ]]
    then 
        echo -e "${GREEN}Droit de lecture des fichiers : check"
    else
        echo -e "${RED}Droit de lecture des fichiers : uncheck"
    fi

    # il peut modifier un fichier dans dir_b
    if [[ -w $f ]]
    then 
        echo -e "${GREEN}Droit d'ecriture des fichiers : check"
    else
        echo -e "${RED}Droit d'ecriture des fichiers : uncheck"
    fi

    break
done

