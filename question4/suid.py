import os
import sys


if(len(sys.argv) < 2) : 
    print("Missing argument");
    quit() 

file = str(sys.argv[1])
print(os.getuid());
print(os.getgid());
print(os.geteuid());
print(os.getegid());

try :
    open(file);
except PermissionError : 
    print("Cannot open file")
    quit()

print("File open correctly")

quit()