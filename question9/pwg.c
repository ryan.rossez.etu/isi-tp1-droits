#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>


char * encrypt(char * message){
    char * encrypted = malloc(strlen(message));

    for(int i = 0 ; i < (int)strlen(message); i++){
        encrypted[i] = message[i]-5;
    }

    return encrypted;
}

char * crypted(char * message){
   char * crypted = malloc(strlen(message));

    for(int i = 0 ; i < (int)strlen(message); i++){
        crypted[i] = message[i]+5;
    }

    return crypted;
}

int main(int argc , char * argv[]){

    if(argc < 3){
        fprintf(stderr,"Erreur : veuillez mettre votre login et mots de passe dans la commmande\n ");
        fprintf(stderr,"Usage : ./pwg username password\n ");
        exit(EXIT_FAILURE);
    }

    // On ouvre le fichier passwd en lecture
    FILE * passwd;
    passwd = fopen("/home/admin/passwd", "r");
    if(passwd == NULL){
        fprintf(stderr, "Erreur : Incapable d'ouvrir le fichier /home/admin/passwd \n");
        return -1;
    }

    // On créer un nouveau fichier dans lequelle on va écrire 
    FILE * newpasswd;
    newpasswd = fopen("/home/admin/newpasswd","a+");
    if(newpasswd == NULL){
        fprintf(stderr, "Erreur : Incapable d'ouvrir le fichier /home/admin/newpasswd %d\n",errno);
        return -1;
    }

    // On regarde si l'utilisateur a deja un mot de passe dans le fichier passwd 
    char * username = argv[1];
    char * oldPassword = malloc(50);
    int hasOldPassword = 0;
    int try = 3;
    int passcheck = 0;
    char enterOldPassword[50];
    // On parcours le fichier pour voir si son login existe 
    char buffer[50];
    while ( ! feof( passwd) ) {
        fgets( buffer, 50 , passwd );
        if ( ferror( passwd ) ) {
            fprintf( stderr, "Erreur : Probléme lors de la lecture du fichier /home/admin/passwd , code de retour %d\n", errno );
            break;
        }

        char * line = malloc(50);
        line= strcpy(line,buffer);
        char *ptr = strtok(buffer, " ");
        if(ptr != NULL){   
            // Si le login existe on récupere le mot de passe 
            if(strncmp(username,ptr,strlen(username)) == 0){
                char * tmp = strtok(NULL," ");
                oldPassword = strcpy(oldPassword,tmp);
                hasOldPassword = 1;
            }else if(! feof(passwd)) {
                //On écrit chaque ligne si elle ne correspond pas au login
                fprintf(newpasswd,"%s",line);
            }
        }
        
    }
    // On saisie l'ancien mot de passe et on verifie si c'est le bon
    while(hasOldPassword && !passcheck && try > 0){
        fprintf(stdout,"Veuillez saisir votre ancien mot de passe : \n");
        scanf("%s",enterOldPassword);
        
        if(strncmp(encrypt(oldPassword),enterOldPassword,strlen(enterOldPassword))){
            fprintf(stderr,"ERREUR : Mot de passe incorrect veuillez recommencer \n");
            try = try - 1;
        }else{
            passcheck = 1;
        }
    }

    if(try < 3){
        fprintf(stderr,"3 éssaies ratées, Identification échouée \n");
        return -1;
    }
    
    // On ecrit la ligne de login + mdp 

    // Si elle n'existeais pas elle se rajoute tout juste a la suite des autres 
    // Si elle existait deja comme elle n'est pas ecrite dans le nouveau fichier 
    // Elle ne sera pas doublons elle sera juste deplacé a la fin du fichier 
    fprintf(newpasswd,"%s %s\n",username,crypted(argv[2]));
    
    //On supprime l'ancien fichier 
    remove("/home/admin/passwd");
    // On renome le nouveau fichier 
    rename("/home/admin/newpasswd","/home/admin/passwd");

    return 0;    
}