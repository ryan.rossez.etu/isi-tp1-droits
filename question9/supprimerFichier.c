#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdlib.h>

char * encrypt(char * message){
    char * encrypted = malloc(strlen(message));

    for(int i = 0 ; i < (int)strlen(message); i++){
        encrypted[i] = message[i]-5;
    }

    return encrypted;
}

int main(int argc , char * argv[]){

    if(argc < 2){
        fprintf(stderr, "Erreur : paramêtre manquant, veuillez précisé le nom du fichier que vous voulez supprimer !\n");
        return -1;
    }

    char * filename = argv[1];

    fprintf(stdout,"Suppression du fichier %s en cours ... \n",filename);

    // RECUPERATION DU GROUPE DE LA PERSONNE QUI EXECUTE LE PROGRAMME
    int groupeUser = getegid();
    // RECUPEATION DU GROUPE DU FICHIER QUE L'ON ESSAIE DE SUPPRIMER
    FILE *f;
    f = fopen(filename, "rx");
    if(f == NULL){
        fprintf(stderr, "Erreur : Incapable d'ouvrir le fichier a supprimer, vous n'avez pas les droit \n");
        return -1;
    }

    struct stat sb;

    if (stat(filename, &sb) == -1) {
        fprintf(stderr,"Erreur : incapable de récupérer les informations du fichier \n");
        return -1;         
    }

    int groupeFile = sb.st_gid;

    // COMPARAISON DES GROUPES
    if(groupeFile != groupeUser){
        fprintf(stderr,"Erreur : Le fichier n'a pas le même groupe que l'utilisateur, Interdit de continuer : %d != %d \n",groupeUser,groupeFile);
        return -1;
    }

    int essaie = 3;
    int check = 0;
    char password[50];
    char login[50];

    while(!check && essaie > 0){
        // SAISIE DU LOGIN/MDP DE L'UTILISATEUR
        fprintf(stdout,"Veuillez saisir votre nom d'utilisateur : \n");
        scanf("%s", login);
        
        fprintf(stdout,"Veuillez saisir votre mot de passe : \n");
        scanf("%s",password);

        // RECUPERATION DES LOGIN MOT DE PASSE DANS LE FICHIER /home/admin/passwd
        FILE *passwd;
        passwd = fopen("/home/admin/passwd", "r");
        if(passwd == NULL){
            fprintf(stderr, "Erreur : Incapable d'ouvrir le fichier /home/admin/passwd \n");
            return -1;
        }

        // COMPARAISON DES LOGIN ET MDP 

        char buffer[50];
        while ( ! feof( passwd) ) {
            fgets( buffer, 50 , passwd );
            if ( ferror( passwd ) ) {
                fprintf( stderr, "Erreur : Probléme lors de la lecture du fichier /home/admin/passwd , code de retour %d\n", errno );
                break;
            }

            char *ptr = strtok(buffer, " ");
            if(ptr != NULL){
                if(strncmp(login,ptr,strlen(login)) == 0){
                    ptr = strtok(NULL," ");
                    if(ptr != NULL){
                        if(strncmp(encrypt(password),ptr,strlen(password))){
                            fprintf(stderr,"ERREUR : Mot de passe incorrect veuillez recommencer \n");
                            essaie = essaie - 1;
                        }else{
                            check = 1;
                        }
                    }else{
                        essaie = essaie - 1;
                    }
                }    
            }else{
                essaie = essaie - 1;
            }
            
        } 
    }

    if(essaie < 3){
        fprintf(stderr,"3 éssaies ratées, Identification échouée \n");
        return -1;
    }

    fprintf(stdout,"Identification réussie ... \n");

    // SUPPRESSION DU FICHIER DANS LE DIR_A ou DIR_B
    if(remove(filename)){
        fprintf(stderr,"ERREUR : Erreur lors de la suppression du fichier");
    }

}