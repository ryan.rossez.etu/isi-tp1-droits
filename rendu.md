# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Rossez, Ryan, email: ryan.rossez.etu@univ-lille.fr  
  
- Prognon, Quentin, email: quentin.prognon.etu@univ-lille.fr  

## Question 1

non il ne peut pas écrire car toto est le propriétaire   
Et dans les droits on remarque que le propriétaire n'a pas les droit d'écrire   
il a seulement ceux de lecture  

## Question 2

```bash
mkdir mydir
chmod mydir -x
cd mydir
```
Le droit d'execution sur un dossier empeche le fais de l'ouvrir.  
On ne peux donc plus rentrer dedans car on a retirer le droit d'execution  

```bash
touch mydir/data.txt  
ls -al mydir   
```

On peux voir la liste des fichiers présent dans le dossier   
Mais par contre toute les informations sont masquée a l'exeption du type de fichier  
Tout les droit et autre informations sont remplacée par des points d'interrogation
Car on a pas les droits d'acceder au dossier.  
  
![](screenshot/lsalwithoutexecute.png)

## Question 3

Sans set-user-id :   

```
./suid ./../mydir/data.txt  
EUID 1002  
EGID 1002  
RUID 1002   
RGID 1002  
Hello world  
Cannot open file: Permission denied  
```

Avec set-user-id :   

```
./suid ./../mydir/data.txt  
EUID 1000  
EGID 1002  
RUID 1002  
RGID 1002  
Hello world  
File open correctly  
```

On remarque donc que au niveau du EUID il n'est pas le même avec le droit privilégié.   
On peut imaginer que l'EUID 1000 correspond a l'UID du root Ce qui lui donne le droit d'accéder au fichier dans tout les cas   

## Question 4

Sans set-user-id :

```
python3 suid.py ./../mydir/data.txt
1002
1002
1002
1002
Cannot open file
```

Avec set-user-id : 

```
python3 suid.py ./../mydir/data.txt
1002
1002
1002
1002
Cannot open file
```
Que ce soit avec ou sans set-user-id le fichier python n'arrive pas a accéder au dossier 

Nous supposons que comme le fichier python n'est pas un executable il n'arrive pas a utiliser l'EUID de l'utilisateur ROOT 
et donc n'arrive pas a charger le dossier car il n'y a pas les droit d'execution

## Question 5

chfn permet de modifier le nom complet et les informations associées à un utilisateur

On vois que le ficher /usr/bin/chfn a un set-user-id de mis 
L'utilisateur a donc tout les droit avec le set-user-id au niveau du droit d'execution.   
puis pour le groupe et les autres il n'y a que les droit de lecture et d'execution 

Dans le fichier /etc/passwd on a bien une modification de l'utilisateur. On peut remarquer le numero de bureau et de telephone dans les informations au niveau du /etc/passwd


## Question 6

Les mots de passe sont stockée dans le fichier /etc/shadow
Et tout les mots de passe sont cryptée pour évitée de pouvoir accéder trop facilement au mot de passe
Pour que seulement le pc soit capable de comprendre les mots de passe et pas les personnes qui l'utilise


## Question 7

# Liste de toute les commandes effectuée pour la création des user et group   

addgroup groupe_a  
addgroup groupe_b  
addgroup admin  
adduser lambda_a   
adduser lambda_b  
adduser lambda_a groupe_a  
adduser lambda_b groupe_b  
sudo usermod --gid groupe_a lambda_a  
sudo usermod --gid groupe_b lambda_b  
sudo delgroup lambda_a   
sudo delgroup lambda_b  
adduser admin  
adduser admin admin  
adduser admin groupe_a   
adduser admin groupe_b   
  
# Liste de toute les commandes pour créer les dossier / fichier   
  
Présent dans le script createDir.sh  
(N'est pas executable, juste une liste de commande a taper une a une dans le terminal)  
  
# Script de verification   
  
./verifAdmin.sh -> Script a lancée avec l'utilisateur admin pour vérifier si tout les droit sont mis correctement   
  
./verifLambda.sh \[groupe\] -> groupe a chosir entre a et b par rapport a si l'utilisateur fais partie du groupe a ou b   
  
## Question 8

Création du programme C avec l'utilisateur admin et ajout du setuserid 
Pour qu'il ai les même droit que l'admin et donc le droit de supprimer les fichiers 
dans les dossier dir_a et dir_b 

./rmg \[filepath\]

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Programme fonctionnel seulement l'utilisateur Admin 

-> Pas réussie a le rendre utilisable par tout le monde 
Même en mettant le setuserid sur le fichier pwg.c et l'executable 
sur aucun des deux ca fonctionne 

./pwg \[username\] \[password\]

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Non effectuée 








